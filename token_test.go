package gotp

import (
	"testing"
	"time"
)

func TestNewHotpToken(t *testing.T) {
	token := NewHotpToken("GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ", 0)
	// Calling Value() on a HOTP token should increment its counter.
	for _, data := range hotpTests {
		if data.counter != token.Counter {
			t.Errorf("expected counter: %d, actual value: %d", data.counter, token.Counter)
		}
		value, _ := token.Value()
		if value != data.token {
			t.Errorf("expected token: %s, actual value: %s", data.token, value)
		}
	}
	// ValueFor() should use the supplied counter value rather than the internal one.
	for _, data := range hotpTests {
		value, _ := token.ValueFor(data.counter)
		if value != data.token {
			t.Errorf("expected token: %s, actual value: %s", data.token, value)
		}
	}
}

func TestNewTotpToken(t *testing.T) {
	token := NewTotpToken("GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ")
	token.Digits = 8
	for _, data := range totpTests {
		value, _ := token.ValueAt(time.Unix(data.counter, 0))
		if value != data.token {
			t.Errorf("expected token: %s, actual value: %s", data.token, value)
		}
	}
}

func TestTokenInit(t *testing.T) {
	token := &Token{}
	token.Init()

	if token.Algorithm != DefaultAlgorithm {
		t.Errorf("expected algorithm: %s, actual value: %s", DefaultAlgorithm, token.Algorithm)
	}

	if token.Digits != DefaultDigits {
		t.Errorf("expected digits: %d, actual value: %d", DefaultDigits, token.Digits)
	}

	if token.Period != DefaultPeriod {
		t.Errorf("expected period: %d, actual value: %d", DefaultPeriod, token.Period)
	}

	if token.Type != "totp" {
		t.Errorf("expected type: %s, actual value: %s", "totp", token.Type)
	}

	token = &Token{
		Algorithm:  "sha512",
		Digits: 1,
		Period: 1,
		Type: "hotp",
	}

	token.Init()

	if token.Algorithm != "sha512" {
		t.Errorf("expected algorithm: %s, actual value: %s", "sha512", token.Algorithm)
	}

	if token.Digits != 1 {
		t.Errorf("expected digits: %d, actual value: %d", 1, token.Digits)
	}

	if token.Period != 1 {
		t.Errorf("expected period: %d, actual value: %d", 1, token.Period)
	}

	if token.Type != "hotp" {
		t.Errorf("expected type: %s, actual value: %s", "hotp", token.Type)
	}
}
