package gotp

import (
	"encoding/hex"
	"fmt"
	"testing"
)

type otpTest struct {
	counter int64
	secret  string
	hmac    string
	token   string
	digits  int
}

var (
	// From HOTP RFC, https://tools.ietf.org/html/rfc4226
	hotpTests = []otpTest{
		otpTest{
			counter: 0,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "cc93cf18508d94934c64b65d8ba7667fb7cde4b0",
			token:   "755224",
		},
		otpTest{
			counter: 1,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "75a48a19d4cbe100644e8ac1397eea747a2d33ab",
			token:   "287082",
		},
		otpTest{
			counter: 2,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "0bacb7fa082fef30782211938bc1c5e70416ff44",
			token:   "359152",
		},
	}

	//From TOTP RFC, https://tools.ietf.org/html/rfc6238
	totpTests = []otpTest{
		otpTest{
			counter: 59,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "75a48a19d4cbe100644e8ac1397eea747a2d33ab",
			token:   "94287082",
		},
		otpTest{
			counter: 1111111109,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "278c02e53610f84c40bd9135acd4101012410a14",
			token:   "07081804",
		},
		otpTest{
			counter: 1111111111,
			secret:  "GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ",
			hmac:    "b0092b21d048af209da0a1ddd498ade8a79487ed",
			token:   "14050471",
		},
	}
)

func TestGetSeed(t *testing.T) {
	for _, data := range hotpTests {
		seed_bytes, _ := GetSeed(data.secret, data.counter, "sha1")
		seed := fmt.Sprintf(
			"%x", seed_bytes,
		)

		if seed != data.hmac {
			t.Errorf("expected seed: %s, actual value: %s", data.hmac, seed)
		}
	}
	for _, data := range totpTests {
		counter := data.counter / 30
		seed_bytes, _ := GetSeed(data.secret, counter, "sha1")
		seed := fmt.Sprintf(
			"%x", seed_bytes,
		)
		if seed != data.hmac {
			t.Errorf("expected seed: %s, actual value: %s", data.hmac, seed)
		}
	}
}

func TestGetValue(t *testing.T) {
	for _, data := range hotpTests {
		seed, _ := hex.DecodeString(data.hmac)
		token := GetValueFromSeed(seed, 6)
		if token != data.token {
			t.Errorf("expected otp token: %s, actual value: %s", data.token, token)
		}
	}
	for _, data := range totpTests {
		seed, _ := hex.DecodeString(data.hmac)
		token := GetValueFromSeed(seed, 8)
		if token != data.token {
			t.Errorf("expected otp token: %s, actual value: %s", data.token, token)
		}
	}

}
