/*

Copyright (C) 2018 iwaseatenbyagrue

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package gotp

import (
	"time"
)

type Token struct {
	// An optional, human-friendly name for the token
	Name string `mapstructure:"name"`
	// Token secret, a base32 encoded string.
	Secret string `mapstructure:"secret"`
	// Hash algorithm to derive OTP value with
	Algorithm string `mapstructure:"algorithm"`
	// Counter value for HOTP, also used as the 'initial timestamp' for TOTP.
	Counter int64 `mapstructure:"counter"`
	// Validity/renewal period for TOTP token
	Period int64 `mapstructure:"period"`
	// Type of token, required only for HOTP tokens
	Type string `mapstructure:"type"`
	// Whether to include a checksum digit in token
	Checksum bool `mapstructure:"checksum"`
	// Number of digits the OTP value should contain
	Digits int `mapstructure:"digits"`
}

func NewTotpToken(secret string) *Token {
	return &Token{
		Secret:    secret,
		Type:      "totp",
		Algorithm: DefaultAlgorithm,
		Digits:    DefaultDigits,
		Period:    DefaultPeriod,
	}
}

func NewHotpToken(secret string, counter int64) *Token {
	return &Token{
		Counter:   counter,
		Secret:    secret,
		Type:      "hotp",
		Algorithm: DefaultAlgorithm,
		Digits:    DefaultDigits,
		Period:    DefaultPeriod,
	}
}

/*
 * Generate an OTP password.
 *
 * For HOTP tokens, this will also increment the Counter value.
 */
func (token *Token) Value() (value string, err error) {

	switch token.Type {
	case "hotp":
		value, err = token.ValueFor(token.Counter)
		if err == nil {
			token.Counter++
		}
		return value, err
	default:
		return token.ValueAt(time.Now())
	}

	return
}

/*
 * Generate a TOTP password from a timestamp.
 *
 */
func (token *Token) ValueAt(timestamp time.Time) (value string, err error) {
	return token.ValueFor(GetCounterFromTime(timestamp, token.Period, token.Counter))
}

/*
 * Generate a HOTP password for a given counter value.
 *
 */
func (token *Token) ValueFor(counter int64) (value string, err error) {
	seed, err := GetSeed(token.Secret, counter, token.Algorithm)

	if err != nil {
		return "", err
	}

	switch token.Checksum {
	case true:
		value, err = GetValueFromSeedWithChecksum(seed, token.Digits)
	default:
		value = GetValueFromSeed(seed, token.Digits)
	}

	return
}


/*
 * Initialise token fields using defaults if zero value is present.
*/
func (token *Token) Init() {
	if token.Period == 0 {
		token.Period = DefaultPeriod
	}

	if token.Digits == 0 {
		token.Digits = DefaultDigits
	}

	if token.Algorithm == "" {
		token.Algorithm = DefaultAlgorithm
	}

	if token.Type == "" {
		token.Type = "totp"
	}
}
