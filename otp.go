/*

Copyright (C) 2018 iwaseatenbyagrue

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package gotp

import (
	"crypto/hmac"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base32"
	"fmt"
	"hash"
	"math"
	"strings"
	"time"
)

func getHashFunction(algo string) func() hash.Hash {
	switch algo {
	case "sha256":
		return sha256.New
	case "sha512":
		return sha512.New
	default:
		return sha1.New
	}

}

func CounterToBytes(counter int64) []byte {
	counter_bytes := make([]byte, 8)
	for x := len(counter_bytes) - 1; x >= 0; x-- {
		counter_bytes[x] = byte(counter & 0xff)
		counter >>= 8
	}
	return counter_bytes
}

func GetCounterFromTime(timestamp time.Time, period, offset int64) int64 {
	return (timestamp.Unix() - offset) / period
}

func GetSeed(secret string, counter int64, algo string) ([]byte, error) {
	secret_bytes, err := base32.StdEncoding.DecodeString(strings.ToUpper(secret))
	if err != nil {
		return secret_bytes, err
	}
	h := hmac.New(getHashFunction(algo), secret_bytes)
	_, err = h.Write(CounterToBytes(counter))
	if err != nil {
		return nil, err
	}
	return h.Sum(nil), err
}

func GetValueFromSeed(seed []byte, digits int) (token string) {
	var offset, otp_token int

	offset = int(seed[len(seed)-1] & 0xf)

	otp_token = int(seed[offset%len(seed)]&0x7f) << 24
	otp_token |= int(seed[(offset+1)%len(seed)]&0xff) << 16
	otp_token |= int(seed[(offset+2)%len(seed)]&0xff) << 8
	otp_token |= int(seed[(offset+3)%len(seed)] & 0xff)

	token = fmt.Sprintf("%d", otp_token%int(math.Pow10(digits)))
	for x := 0; len(token) < digits; x++ {
		token = "0" + token
	}

	return
}

func GetValueFromSeedWithChecksum(seed []byte, digits int) (token string, err error) {
	token = GetValueFromSeed(seed, digits)
	checksum, err := CalculateLuhnChecksum(token)
	if err != nil {
		return token, err
	}
	token = fmt.Sprintf("%s%d", token, checksum)

	return token, err
}
