package gotp

const (
	DefaultAlgorithm       = "sha1"
	DefaultDigits          = 6
	DefaultPeriod    int64 = 30
)
