package gotp

import (
	"testing"
)

func TestValidateLuhnChecksum(t *testing.T) {
	// From https://en.wikipedia.org/wiki/Luhn_algorithm
	if ok, _ := ValidateLuhnChecksum("79927398713"); !ok {
		t.Errorf("failed to validate checksum")
	}

	// Check padding doesn't affect result
	if ok, _ := ValidateLuhnChecksum("079927398713"); !ok {
		t.Errorf("failed to validate 0-padded checksum")
	}

	if ok, _ := ValidateLuhnChecksum("79927398710"); ok {
		t.Errorf("failed to detect invalid checksum")
	}

	// Error on invalid string
	if _, err := ValidateLuhnChecksum("7992A7398710"); err == nil {
		t.Errorf("failed to error on invalid input")
	}

}

func TestCalculateLuhnChecksum(t *testing.T) {
	// From https://en.wikipedia.org/wiki/Luhn_algorithm
	if result, _ := CalculateLuhnChecksum("7992739871"); result != 3 {
		t.Errorf("failed to calculate checksum digit")
	}
	// Check padding doesn't affect result
	if result, _ := CalculateLuhnChecksum("07992739871"); result != 3 {
		t.Errorf("failed to calculate checksum from 0-padded string")
	}

	if result, _ := CalculateLuhnChecksum("7992739871"); result == 0 {
		t.Errorf("failed to calculate correct checksum digit")
	}

	// Error on invalid string
	if result, err := CalculateLuhnChecksum("7992A7398710"); err == nil {
		t.Errorf("failed to error on invalid input: %d", result)
	}
}
