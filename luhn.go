/*

Copyright (C) 2018 iwaseatenbyagrue

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package gotp

import (
	"strconv"
)

func CalculateLuhnChecksum(token string) (result int, err error) {
	var (
		sum, parity int
	)

	parity = len(token) % 2

	for i := len(token); 0 < i; i-- {
		digit, err := strconv.Atoi(string(token[i-1]))

		if err != nil {
			return result, err
		}

		if i%2 == parity {
			digit = digit * 2
			if 9 < digit {
				digit = (digit % 10) + 1
			}
		}
		sum += digit
	}

	result = sum % 10

	if 0 < result {
		result = 10 - result
	}

	return
}

func ValidateLuhnChecksum(token string) (result bool, err error) {
	var (
		sum, parity int
	)

	parity = (len(token) - 1) % 2

	for i := len(token); 0 < i; i-- {
		digit, err := strconv.Atoi(string(token[i-1]))

		if err != nil {
			return result, err
		}

		if i%2 == parity {
			digit *= 2
			if digit > 9 {
				digit = (digit % 10) + 1
			}
		}
		sum += digit
	}

	result = sum%10 == 0

	return
}
