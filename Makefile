.PHONY: clean coverage test

BUILD_DATE ?= $(shell date --iso-8601=s )
BUILD_ID ?= $(shell date +"%s")
BUILD_FLAGS ?= -extldflags '-static' -X $(MODULE)/gotp/cmd.version=$(VERSION) -X $(MODULE)/gotp/cmd.vcsRef=$(VCS_REF) -X $(MODULE)/gotp/cmd.buildDate=$(BUILD_DATE) -X $(MODULE)/gotp/cmd.buildId=$(BUILD_ID)
GOBIN ?= ~/bin
MODULE ?= $(shell grep module go.mod | cut -d' ' -f2 )
VCS_REF ?= $(shell git rev-parse --short HEAD)
VERSION ?= $(VCS_REF)
coverage:
	go test -cover ./...

test:
	go test ./...

build: GOBIN = $(shell pwd)/build/
build: build/$(shell basename `pwd`)

clean:
	rm -rf build release

install: $(GOBIN)/$(shell basename `pwd`)

release: VERSION=$(shell git describe --tags | head -n1 )
release: clean release/gotp-linux-amd64 release/gotp-darwin-amd64
	(cd release && sha256sum gotp* > gotp.SHA256SUMS)

release/gotp-%-amd64: $(shell find . -name '*.go' )
	(test -d release || mkdir -p release)
	(CGO_ENABLED=0 GOOS=$* GOARCH=amd64 go build -o release/gotp-$*-amd64 -ldflags "$(BUILD_FLAGS)" gotp/main.go)


%/$(shell basename `pwd`) : $(shell find . -name '*.go' )
	mkdir -p $(GOBIN)
	(GOBIN=$(GOBIN) CGO_ENABLED=0 go install -ldflags "$(BUILD_FLAGS)"  ./... )
