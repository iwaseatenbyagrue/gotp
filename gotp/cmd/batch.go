package cmd

import (
	"fmt"
  "os"
  "path/filepath"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/iwaseatenbyagrue/gotp"
  homedir "github.com/mitchellh/go-homedir"
)

type BatchConfig struct {
  Tokens []*gotp.Token `mapstructure:"tokens"`
}

var (
	batchCmd = &cobra.Command{
		Use:     "batch",
		Aliases: []string{"b"},
		Short:   "Generate codes from a configuration file.",
		Long:    "Generate OTP values for tokens defined in a configuration file.",
		Run: func(cmd *cobra.Command, args []string) {
      batch := &BatchConfig{}
      configPath, err := homedir.Expand(filepath.Clean(args[0]))

      if err != nil {
        fmt.Printf("error: %s", err)
        os.Exit(1)
      }

      v := viper.New()
      v.SetConfigName(filepath.Base(configPath))
      v.AddConfigPath(filepath.Dir(configPath))

      if filepath.Ext(configPath) == "" {
        v.SetConfigType("yaml")
      } else {
        v.SetConfigType(filepath.Ext(configPath)[1:])
      }

      if err = v.ReadInConfig(); err != nil {
          fmt.Fprintf(os.Stderr, "failed to read configuration: %s\n", err)
          os.Exit(1)
      }
      if err = v.Unmarshal(batch); err != nil {
        fmt.Fprintf(os.Stderr, "failed to unnmarshal configuration: %s\n", err)
        os.Exit(1)
      }

      for idx, token := range batch.Tokens {
        // Set defaults for token.
        token.Init()

        name := token.Name
        if name == "" {
          name = fmt.Sprintf("#%d", idx)
        }

        value, err := token.Value()

        if err != nil {
          fmt.Fprintf(os.Stderr, "failed to get token for %s: %s", name, err)
        } else {
          fmt.Fprintf(os.Stdout, "%s: %s\n", name, value)
        }

      }

      if viper.GetBool("no-update") == false {
        v.Set("tokens", batch.Tokens)
        v.WriteConfig()
      }
		},
    Args: cobra.ExactArgs(1),
	}
)

func init() {
  batchCmd.Flags().BoolP("no-update", "n", false, "Don't update configuration file (WARNING: if you have HOTP tokens, you will need to update their counter manually).")
  viper.SetDefault("no-update", false)
  viper.BindPFlag("no-update", batchCmd.Flags().Lookup("no-update"))
  rootCmd.AddCommand(batchCmd)
}
