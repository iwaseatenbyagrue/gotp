package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/iwaseatenbyagrue/gotp"
	"os"
)

var (
	totpCmd = &cobra.Command{
		Use:     "totp",
		Aliases: []string{"t"},
		Short:   "Generate a TOTP token",
		Run: func(cmd *cobra.Command, args []string) {
			token := gotp.NewTotpToken(viper.GetString("secret"))
			token.Algorithm = viper.GetString("algorithm")
			token.Digits = viper.GetInt("digits")
			token.Period = viper.GetInt64("period")
			value, err := token.Value()
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}
			fmt.Println(value)
		},
	}
)

func init() {
	totpCmd.Flags().Int64P("period", "p", gotp.DefaultPeriod, "Validity period of the token")
	viper.BindPFlag("period", totpCmd.Flags().Lookup("period"))

	rootCmd.AddCommand(totpCmd)
}
