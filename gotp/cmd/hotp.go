package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/iwaseatenbyagrue/gotp"
	"os"
)

var (
	hotpCmd = &cobra.Command{
		Use:     "hotp",
		Aliases: []string{"h", "c"},
		Short:   "Generate a HOTP token",
		Long:    "Generate an OTP token from the provided counter value",
		Run: func(cmd *cobra.Command, args []string) {
			token := gotp.NewHotpToken(viper.GetString("secret"), viper.GetInt64("counter"))
			token.Algorithm = viper.GetString("algorithm")
			token.Digits = viper.GetInt("digits")
			value, err := token.Value()
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s\n", err)
				os.Exit(1)
			}
			fmt.Println(value)
		},
	}
)

func init() {
	viper.SetDefault("counter", 0)
	hotpCmd.Flags().Int64P("counter", "c", 0, "HOTP counter value (env: GOTP_COUNTER)")
	viper.BindPFlag("counter", hotpCmd.Flags().Lookup("counter"))
	rootCmd.AddCommand(hotpCmd)
}
