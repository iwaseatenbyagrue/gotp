/*

Copyright (C) 2018 iwaseatenbyagrue

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/iwaseatenbyagrue/gotp"
)

var (
	algorithm string
	secret    string
	rootCmd   = &cobra.Command{
		Use:   "gotp",
		Short: "An OTP token generator.",
		Long:  "Generate (T|H)OTP tokens on the command line.",
	}
)

func Execute() {
	rootCmd.Execute()
}

func init() {
	viper.SetDefault("algorithm", gotp.DefaultAlgorithm)
	viper.SetDefault("digits", gotp.DefaultDigits)
	viper.SetDefault("secret", "")
	rootCmd.PersistentFlags().StringP("algorithm", "a", gotp.DefaultAlgorithm, "Hashing algorithm for token derivation (env: GOTP_ALGORITHM)")
	rootCmd.PersistentFlags().IntP("digits", "d", gotp.DefaultDigits, "Number of digits in token (env: GOTP_DIGITS)")
	rootCmd.PersistentFlags().StringP("secret", "s", "", "Token secret (env: GOTP_SECRET)")
	viper.BindPFlags(rootCmd.PersistentFlags())
	viper.SetEnvPrefix("gotp")
	viper.AutomaticEnv()
}
