package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var (
	buildDate  string
	buildId    string
	vcsRef     string
	version    string
	versionCmd = &cobra.Command{
		Use:     "version",
		Aliases: []string{"v"},
		Short:   "Display version info",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("%12s : %s\n", "version", version)
			fmt.Printf("%12s : %s\n", "vcs ref", vcsRef)
			fmt.Printf("%12s : %s\n", "build date", buildDate)
			fmt.Printf("%12s : %s\n", "build id", buildId)
		},
	}
)

func init() {
	rootCmd.AddCommand(versionCmd)
}
