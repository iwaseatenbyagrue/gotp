# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2021-03-01

### Added

* `batch` command, to get values for one or more tokens defined in a configuration file.
  To safely support HOTP tokens, it will update the configuration file in place, recording
  the incremented counter value.
* `gotp.Token` now has an `Init()` method that update the token to use defaults in
  place of zero values for several fields.
* `.gitignore` based on [github's template for Go](https://github.com/github/gitignore/blob/master/Go.gitignore)

## [0.1.3] - 2021-02-28

### Added

* Defaults for `gotp.Token` and token generation more generally are defined as constants.
* `Makefile` with targets for testing, building, installing binary locally, and releasing.
* CLI arguments can be provided via env vars.

### Changed

* CLI is now built on top of [cobra](https://github.com/spf13/cobra) and [viper]((https://github.com/spf13/viper).
* refactored and simplified `gotp.Token`, now reduced to 3 functions providing token values.
* partial refactor of OTP functions

### Removed

* `gotp.TokenConfig` has been deprecated and removed, with most fields moved to `gotp.Token`

## [0.1.2] - 2018-12-23

### Changed

* Fixed bug in `gotp totp` that prevented obtaining tokens other than for the current time.

## [0.1.1] - 2018-12-20

### Added

* No longer silently ignore errors around Luhn checksum and seed generation

### Changed

* More compact version display for CLI
* Removed version info from usage

## [0.1.0] - 2018-12-18

### Added

* CLI can display version info

## [0.0.1] - 2018-12-18

Initial release for gotp.
This provides a small set of functions needed to generate OTP tokens/codes, and a simplistic implementation of Luhn checksum algorithm.
A basic command line utility allowing generation of tokens is provides as a binary.

### Added

* (H|T)OTP code generation functions
* Luhn checksum calculation and validation
* Basic CLI

[0.1.3]: https://gitlab.com/iwaseatenbyagrue/gotp/-/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/iwaseatenbyagrue/gotp/-/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/iwaseatenbyagrue/gotp/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/iwaseatenbyagrue/gotp/-/compare/0.0.1...0.1.0
[0.0.1]: https://gitlab.com/iwaseatenbyagrue/gotp/-/releases/0.0.1
